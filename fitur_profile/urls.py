from django.conf.urls import url
from .views import index, edit_profile, add_expertise, delete_expertise, update_profile, save_data, cancel_save_data

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit/$', edit_profile, name='edit_profile'),
    url(r'^update-profile/$', update_profile, name='update–profile'),
	url(r'^add-expertise/$', add_expertise, name='add-expertise'),
	url(r'^delete-expertise/(?P<expertise_id>[0-9]+)/$', delete_expertise, name='delete-expertise'),
	url(r'^save-data/$', save_data, name='save-data'),
	url(r'^cancel-save-data/$', cancel_save_data, name='cancel-save-data'),
]