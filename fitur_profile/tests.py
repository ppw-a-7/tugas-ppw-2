from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, edit_profile, add_expertise, is_expertise_taken, delete_expertise, update_profile, save_data, cancel_save_data
from .models import Mahasiswa, Expertise, Post
import environ
import json


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# def createUser(kode_identitas):
# 	return Mahasiswa.objects.create(username = "username", npm = kode_identitas, name = "name", angkatan = 2018)

class ProfileUnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_profile_url_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 302)

	def test_profile_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, index)

	def test_login_redirect_to_profile(self):
		#logged in, redirect to profile page
		response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('fitur_profile/profile.html')

	# def test_profile_post_success(self):
	# 	response = Client().get('/profile/')
	# 	response_post = Client().post('/profile/add-expertise/', {'new_expertise': 'Java', 'level': 'Beginner'})
	# 	self.assertEqual(response_post.status_code, 200)
	# 	self.assertEqual(Expertise.objects.all().count(), 1)