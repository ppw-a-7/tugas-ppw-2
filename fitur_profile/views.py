# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from .models import Mahasiswa, Expertise
from django.views.decorators.csrf import csrf_exempt
from fitur_status.models import Status

def index(request):
	print ("#==> masuk index")
	if 'user_login' in request.session:
		mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
		expertises = Expertise.objects.filter(mahasiswa = mahasiswa)

		response = {'mahasiswa' : mahasiswa}
		response['mhs_name'] = request.session['name']
		response['expertises'] = expertises
		response['latest'] = Status.objects.last()
		response['status'] = Status.objects.all()

		html = 'fitur_profile/profile.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/')

def edit_profile(request):
	if 'user_login' in request.session:
		html = 'fitur_profile/edit_profile.html'
		mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
		print(mahasiswa.photo)
		expertises = Expertise.objects.filter(mahasiswa = mahasiswa)
		# for expertise in expertises:
		# 	print(expertise.name, expertise.mahasiswa.name)
		response = {"npm" : request.session['kode_identitas']}
		response['expertises'] = expertises
		response['mahasiswa'] = mahasiswa
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/')

@csrf_exempt
def add_expertise(request):
	if request.method == 'POST':
		new_expertise = request.POST.get('new_expertise', False)
		level = request.POST.get('level', False)
		mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
		if (not is_expertise_taken(new_expertise, mahasiswa)):
			expertise = Expertise(name=new_expertise, level=level, mahasiswa= mahasiswa)
			expertise.save()
			data = model_to_dict(expertise)
			return JsonResponse(data)
		return JsonResponse({'message': 'keahlian sudah ditambahkan'}, status=400)

def is_expertise_taken(name, mahasiswa):
	return (Expertise.objects.filter(name=name, mahasiswa=mahasiswa).count() != 0)

@csrf_exempt
def delete_expertise(request, expertise_id):
	Expertise.objects.filter(id=expertise_id).delete()
	return JsonResponse({'message': 'expertise dihapus dari daftar expertise'}, status=204)

@csrf_exempt
def update_profile(request):
	if request.method == 'POST':
		mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
		first_name = request.POST.get('first_name', False)
		last_name = request.POST.get('last_name', False)
		mahasiswa.name = first_name + " " + last_name
		mahasiswa.photo = request.POST.get('photo', False)
		mahasiswa.email = request.POST.get('email', False)
		mahasiswa.profile_linkedin = request.POST.get('profile_linkedin', False)
		mahasiswa.save()
		data = model_to_dict(mahasiswa)
		return JsonResponse(data)
	return JsonResponse({'message' : 'mahasiswa sudah punya data'}, status=400)

@csrf_exempt
def save_data(request):
	if request.method == 'POST':
		mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
		mahasiswa.show_grade = request.POST.get('show_grade', False)
		mahasiswa.save()
		html = 'fitur_profile/profile.html'
		return render(request, html, response)
	return JsonResponse({'message' : 'mahasiswa gagal save data'}, status=400)

@csrf_exempt
def cancel_save_data(request):
	mahasiswa = Mahasiswa.objects.get(npm = request.session['kode_identitas'])
	Expertise.objects.filter(mahasiswa = mahasiswa).delete()
	html = 'fitur_profile/profile.html'
	return render(request, html, response)
