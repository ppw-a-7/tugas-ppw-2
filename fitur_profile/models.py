from django.db import models
# Create your models here.

class Mahasiswa(models.Model):
	name = models.CharField(max_length=50, default="")
	birth_date = models.DateField(default="2017-01-01")
	username = models.CharField(max_length=50, default="")
	npm = models.IntegerField()
	angkatan = models.IntegerField()
	gender = models.CharField(max_length=10, default="")
	email = models.EmailField(max_length=70, default="")
	profile_linkedin = models.CharField(max_length=70, default="")
	photo = models.URLField(default="https://www.vccircle.com/wp-content/uploads/2017/03/default-profile.png")
	show_grade = models.BooleanField(default=False)
	last_post = models.CharField(max_length=140, default="")
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
 		return self.name + ' ' + str(self.npm)


class Expertise(models.Model):
	name = models.CharField(max_length=20, default="")
	level = models.CharField(max_length=20, default="")
	mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE, related_name='expertises')

	def __str__(self):
 		return self.name + ' from ' + self.mahasiswa.name

class Post(models.Model):
	message = models.CharField(max_length=140, default="")
	created_date = models.DateTimeField(auto_now_add=True)
	mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE, related_name='posts')
