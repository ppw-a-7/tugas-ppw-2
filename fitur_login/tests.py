from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class LoginPageUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_loginpage_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_loginpage_using_index_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)

    def test_login_redirect_to_dashboard(self):
        #logged in, redirect to profile page
        response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, '/dashboard/', status_code=302, target_status_code=200, host=None, msg_prefix='', fetch_redirect_response=True)
        self.assertTemplateUsed('fitur_dashboard/layout/base.html')
