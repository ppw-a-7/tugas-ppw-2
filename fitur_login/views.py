# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect('/dashboard/')
    else:
        html = 'layout/login.html'
        return render(request, html)
