from django.apps import AppConfig


class FiturHomepageConfig(AppConfig):
    name = 'fitur_homepage'
