from django.conf.urls import url
from .views import index, index2, add_status, delete_status
from django.conf.urls import url, include

#url formar app
urlpatterns = [
	url(r'^$', index, name='index'),
    url(r'^(?P<npm>[0-9]+)/$', index2, name='index2'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status', delete_status, name='delete_status'),
]