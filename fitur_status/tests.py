from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, delete_status
from .models import Status
from .forms import Status_Form

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class StatusUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_status_url_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 302)

    def test_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_login_redirect_to_status(self):
        #logged in, redirect to status page
        response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/status/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('fitur_status/status.html')

    def test_model_can_create_status(self):
        #Creating a new activity
        status = Status.objects.create(description="test gan")

        #Retrieving all available activity
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    # def test_status_post_success_and_render_the_result(self):
    #     user = create_user()
    #     test = 'Anonymous'
    #     response_post = Client().post('/status/add_status', {'description': test})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/status/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)

    # def test_status_post_error_and_render_the_result(self):
    #     user = create_user()
    #     test = 'Anonymous'
    #     response_post = Client().post('/status/add_status', {'description': ''})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/status/')
    #     html_response = response.content.decode('utf8')
    #     self.assertNotIn(test, html_response)

    def test_status_delete_success(self):
        status = Status(description="test gan")
        status.save()

        response = Client().post('/status/delete_status', {'id-value': status.id})
        self.assertEqual(Status.objects.all().count(), 0)