from django.apps import AppConfig


class FiturStatusConfig(AppConfig):
    name = 'fitur_status'
