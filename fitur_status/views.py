from django.shortcuts import render
from django.http import HttpResponseRedirect
from fitur_profile.models import Mahasiswa
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):#pragma: no cover
    print("#==> masuk index")
    if 'user_login' in request.session:
        npm = request.session['kode_identitas']
        return HttpResponseRedirect('/status/'+npm)
    else:
        return HttpResponseRedirect('/')

def index2(request, npm):#pragma: no cover
    if 'user_login' in request.session:
        status_id = request.get_full_path().split("/")[-2]

        status = Status.objects.all()[::-1]
        profile = Mahasiswa.objects.get(pk=1)

        response['profile'] = profile
        response['status'] = status
        response['mhs_name'] = request.session['name']
        response['latest'] = Status.objects.last()
        response['verified'] = False
        if npm == status_id:
            response['status_form'] = Status_Form
            response['verified'] = True

        html = 'fitur_status/status.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')

def add_status(request):#pragma: no cover
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def delete_status(request):#pragma: no cover
    status = Status.objects.get(id=int(request.POST.get("id-value", "")))
    status.delete()
    return HttpResponseRedirect('/status/')
