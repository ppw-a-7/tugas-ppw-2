from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    description_attrs = {
        'type': 'text',
        'cols': 60,
        'rows': 3,
        'class': 'status-form-textarea',
        'placeholder':'...',
        'max_length' : 500,
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
