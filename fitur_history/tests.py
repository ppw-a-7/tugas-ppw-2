from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class HistoryUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_history_url_exist(self):
        response = Client().get('/riwayat/')
        self.assertEqual(response.status_code, 302)

    def test_history_using_index_func(self):
        found = resolve('/riwayat/')
        self.assertEqual(found.func, index)

    def test_login_redirect_to_riwayat(self):
        #logged in, redirect to profile page
        response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/riwayat/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('history.html')
