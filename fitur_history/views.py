from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from fitur_profile.models import Mahasiswa

from api.riwayat_api import get_history

response = {}

def index(request):
    print ("#==> masuk history")
    if 'user_login' in request.session:
        npm = request.session['kode_identitas']
        mahasiswa = Mahasiswa.objects.get(npm = npm)
        response['histories'] = get_history().json()
        response['mhs_name'] = request.session['name']
        response['show_grade'] = mahasiswa.show_grade;
        print(mahasiswa.name, mahasiswa.show_grade)
        html = 'history.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')
