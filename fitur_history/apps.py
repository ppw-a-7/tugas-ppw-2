from django.apps import AppConfig


class FiturHistoryConfig(AppConfig):
    name = 'fitur_history'
