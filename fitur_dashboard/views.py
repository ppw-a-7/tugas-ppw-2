# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from fitur_status.models import Status

response = {}
def index(request):
    print ("#==> masuk dashboard")
    if 'user_login' in request.session:
        html = 'fitur_dashboard/layout/base.html'
        response = {'mhs_name' : request.session['name']}
        response['latest'] = Status.objects.last()
        response['status'] = Status.objects.all()
        return render(request, html,response)
    else:
        return HttpResponseRedirect('/')
