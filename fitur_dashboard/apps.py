from django.apps import AppConfig


class FiturDashboardConfig(AppConfig):
    name = 'fitur_dashboard'
