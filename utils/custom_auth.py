from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .csui_helper import get_access_token, verify_user

from fitur_profile.models import Mahasiswa, Expertise

def get_user_object(kode_identitas, username):
	name = get_name(username)
	try:
		return Mahasiswa.objects.get(npm = kode_identitas)
	except:
		username = username.replace('.', '_')
		tahun = str(kode_identitas)[0:2]
		tahun = '20' + tahun
		tahun = int(tahun)
		return Mahasiswa.objects.create(username = username, npm = kode_identitas, name = name, angkatan = tahun)

def get_user_expertise_object(user):
	try:
		return Expertise.objects.filter(mahasiswa = user)
	except:
		return Expertise.objects.create(mahasiswa = user)

def get_name(username):
	return " ".join([word.capitalize() for word in username.split(".")])

#authentication
def auth_login(request):
	print ("#==> auth_login")

	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']

		#call csui_helper
		access_token = get_access_token(username, password)
		if access_token is not None:
			ver_user = verify_user(access_token)
			kode_identitas = ver_user['identity_number']
			role = ver_user['role']

			mahasiswa = get_user_object(kode_identitas, username)
			expertises = get_user_expertise_object(mahasiswa)
			nama = get_name(username)

			# set session
			request.session['user_login'] = username
			request.session['access_token'] = access_token
			request.session['kode_identitas'] = kode_identitas
			request.session['role'] = role
			request.session['name'] = nama
			messages.success(request, "Anda berhasil login")
		else:
			messages.error(request, "Username atau password salah")
	return HttpResponseRedirect('/')

def auth_logout(request):
	print ("#==> auth logout")
	request.session.flush() # menghapus semua session

	messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
	return HttpResponseRedirect('/')
