from django.apps import AppConfig


class FiturFriendConfig(AppConfig):
    name = 'fitur_friend'
