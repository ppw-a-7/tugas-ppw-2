from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class FriendUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_friend_url_redirect_not_login(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code, 302)

    def test_friend_using_index_func(self):
        found = resolve('/friend/')
        self.assertEqual(found.func, index)

    def test_login_redirect_to_friend(self):
        #logged in, redirect to profile page
        response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/friend/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('friend.html')
