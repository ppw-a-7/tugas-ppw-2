from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from fitur_profile.models import Mahasiswa, Expertise, Post

response = {}

def index(request):
    print ("#==> masuk teman")
    if 'user_login' in request.session:
        list_mahasiswa = Mahasiswa.objects.all()
        response['list_mahasiswa'] = list_mahasiswa
        response['mhs_name'] = request.session['name']
        html = 'friend.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')

def get_list_expertise_by_mhs(mhs):
    return Expertise.objects.filter(mahasiswa=mhs)
